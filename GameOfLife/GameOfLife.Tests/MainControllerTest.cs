﻿using GameOfLife.Controllers;
using GameOfLife.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Web.Mvc;
using Moq;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Web.Routing;
using System.Linq;

namespace GameOfLife.Tests
{
    
    
    /// <summary>
    ///This is a test class for MainControllerTest and is intended
    ///to contain all MainControllerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MainControllerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for MainController Constructor
        ///</summary>
        [TestMethod()]
        public void MainControllerConstructorTest()
        {
            //Act
            MainController target = new MainController();
            //Assert
            Assert.IsNotNull(target);
        }

        /// <summary>
        ///A test for NextGeneration
        ///</summary>
        [TestMethod()]
        public void NextGenerationTest()
        {
            //Arrange
            var mockRepository = new Mock<ICellRepository>();
            var mockGameOfLife = new Mock<IGameofLife>();
            var GameStart = DateTime.Now;
            Cell cell1= new Cell(new Coordinates(0,0),0,false,GameStart);
            Cell cell2= new Cell(new Coordinates(1,0),0,false,GameStart);
            Cell[] ListCell = new Cell[]{cell1,cell2};
            mockGameOfLife.Setup(g=> g.NextGenerationGrid())
                .Returns(ListCell);
            mockRepository.Setup(r => r.SaveCell(It.IsAny<Cell[]>())).Verifiable();
            MainController target = new MainController(mockGameOfLife.Object, mockRepository.Object);
            MainController.CurrentGame = new Game(GameStart);
            JsonResult actual;
            //Next Generation
            actual = target.NextGeneration();
            //Assert
            IDictionary<string, object>[] data = ((object[])actual.Data).Select(o => new RouteValueDictionary(o)).ToArray();
            Assert.AreEqual(2,data.Count());
            Assert.AreEqual(0, data[0]["X"]);
            Assert.AreEqual(0, data[0]["Y"]);
            Assert.AreEqual(1, data[1]["X"]);
            Assert.AreEqual(0, data[1]["Y"]);   
        }

        /// <summary>
        ///A test for Start
        ///</summary>

        [TestMethod()]
        public void StartTest()
        {
            //Arrange
            string coor="[{\"X\":\"0\",\"Y\":\"0\"},{\"X\":\"1\",\"Y\":\"0\"}]";
            var mockRepository = new Mock<ICellRepository>();
            var mockGameOfLife = new Mock<IGameofLife>();
            var GameStart = DateTime.Now;
            var Game = new Game(GameStart);
            Coordinates coor1 = new Coordinates(0, 0);
            Coordinates coor2 = new Coordinates(1, 0);
            Coordinates[] ListCoor = new Coordinates[] { coor1, coor2 };
            Cell cell1 = new Cell(new Coordinates(0, 0), 0, false, GameStart);
            Cell cell2 = new Cell(new Coordinates(1, 0), 0, false, GameStart);
            Cell[] ListCell = new Cell[] { cell1, cell2 };

            mockGameOfLife.Setup(g => g.Start(ListCoor, It.IsNotNull<Game>()))
            .Returns(ListCell);

            mockRepository.Setup(r => r.SaveGame(It.IsAny<Game>())).Verifiable();
            //Act
            MainController target = new MainController(mockGameOfLife.Object, mockRepository.Object);
            JsonResult actual;
            actual = target.Start(coor);
            //assert
            IDictionary<string,object> data = new RouteValueDictionary(actual.Data);
            Assert.AreEqual(true, (bool)actual.Data);
        }
        [TestMethod()]
        public void StartReturnFalseTest()
        {
            //Arrange
            string coor = "[{\"Z\":\"0\",\"T\":\"0\"},{\"Z\":\"1\",\"T\":\"0\"}]";
            //Act
            MainController target = new MainController();
            JsonResult actual;
            actual = target.Start(coor);
            //assert
            IDictionary<string, object> data = new RouteValueDictionary(actual.Data);
            Assert.AreEqual(false, (bool)actual.Data);
        }
        [TestMethod()]
        public void IndexTest()
        {
            //Arrange
            MainController target = new MainController();
            ViewResult result = target.Index();
            //Assert
            Assert.AreEqual(64,result.ViewBag.GridRow);
            Assert.AreEqual(64, result.ViewBag.GridColumn);
        }
    }
}
