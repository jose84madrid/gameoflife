﻿using GameOfLife.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Collections.Generic;

namespace GameOfLife.Tests
{
    
    
    /// <summary>
    ///This is a test class for RowTest and is intended
    ///to contain all RowTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RowTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Row Constructor
        ///</summary>
        [TestMethod()]
        public void RowConstructorTest()
        {
            //Act
            Row target = new Row();
            //Assert
            Assert.IsNotNull(target);
        }

        /// <summary>
        ///A test for CellsList
        ///</summary>
        [TestMethod()]
        public void CellsListTest()
        {
            //Act
            Row target = new Row(); 
            //Assert
            Assert.AreEqual(target.CellsList.Count,0);
        }

        /// <summary>
        ///A test for adding a Cell
        ///</summary>
        [TestMethod()]
        public void AddandAccessCellTest()
        {
            //Arrange
            Row target = new Row();
            int y = 0; 
            Cell actual= null;
            //Act 
            target.CellsList.Add(actual);
            actual = target[y];
            Assert.AreEqual(target[y], actual);
            
        }
        /// <summary>
        /// Test ArgumentOutOfRangeException when getting cell
        /// </summary>  
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ExceptiongettingCellTest()
        {
            //Arrange
            Row target = new Row();
            int y = 1;
            Cell actual = null;
            //Act 
            target.CellsList.Add(actual);
            actual = target[1];
        }
        /// <summary>
        /// Test Exception settingCellTest when setting cell
        /// </summary>  
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ExceptionsettingCellTest()
        {
            //Arrange
            Row target = new Row();
            int y = 1;
            Cell actual = null;
            //Act 
            target.CellsList.Add(actual);
            target[y] = actual;
        }
        /// <summary>
        /// Test ExceptiongettingCellTest when setting cell index  <0
        /// </summary>  
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ExceptionsettingCellminuszeroTest()
        {
            //Arrange
            Row target = new Row();
            int y = -1;
            Cell actual = null;
            //Act 
            target.CellsList.Add(actual);
            target[y] = actual;
        }
        /// <summary>
        /// Test ArgumentOutOfRangeException when getting cell
        /// </summary>  
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ExceptiongettingCellminuszeroTest()
        {
            //Arrange
            Row target = new Row();
            int y = -1;
            Cell actual = null;
            //Act 
            target.CellsList.Add(actual);
            actual = target[1];
        }
    }
}
