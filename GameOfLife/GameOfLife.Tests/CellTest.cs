﻿using GameOfLife.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Reflection;

namespace GameOfLife.Tests
{
    
    
    /// <summary>
    ///This is a test class for CellTest and is intended
    ///to contain all CellTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CellTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


/// <summary>
///A test for Cell Constructor
///</summary>
[TestMethod()]
public void CellConstructorTest()
{
    //Arrange
    Coordinates coor = new Coordinates(1,1);
    int Generation = 0; 
    bool IsAlive = false;
    DateTime StartGame = new DateTime();
    StartGame = DateTime.Now;
    //Act
    Cell target = new Cell(coor, Generation, IsAlive, StartGame);
    //Assert
    Assert.IsNotNull(target);
}

/// <summary>
///A test for Coor
///</summary>

[TestMethod()]

public void CoorTest()
{
    //Arrange
    Coordinates coor = new Coordinates(1, 1);
    int Generation = 0; 
    bool IsAlive = false; 
    DateTime StartGame = new DateTime();
    StartGame = DateTime.Now;
    //Act
    Cell target = new Cell(coor, Generation, IsAlive, StartGame); 
    
    //Assert
    Coordinates actual;
    actual = target.Coor;
    Assert.IsTrue(coor.X == actual.X);
    Assert.IsTrue(coor.Y == actual.Y);
}
/// <summary>
/// Check if a exception is thown when coor parametes is null
/// </summary>
[TestMethod()]
[ExpectedException(typeof(NullReferenceException))]
public void CoorNullTest()
{
    //Arrange
    Coordinates coor = null;
    int Generation = 0; 
    bool IsAlive = false; 
    DateTime StartGame = new DateTime();
    StartGame = DateTime.Now;
    //Act
    Cell target = new Cell(coor, Generation, IsAlive, StartGame); 

}
/// <summary>
///A test for X and Y 
///</summary>

[TestMethod()]

public void XYTest()
{
    //Arrange
    Coordinates coor = new Coordinates(1,2);
    int Generation = 0; 
    bool IsAlive = false;
    DateTime StartGame = new DateTime();
    StartGame = DateTime.Now;
    //Act
    Cell target = new Cell(coor, Generation, IsAlive, StartGame);
    //Assert
    PrivateObject pCell = new PrivateObject(target);
    Assert.AreEqual(target.X,1);
    Assert.AreEqual(target.Y, 2);

}

/// <summary>
///A test for checking that StartGame value is store properly
///</summary>

[TestMethod()]
public void startDateTest()
{
    Coordinates coor = new Coordinates(1, 2);
    int Generation = 0; 
    bool IsAlive = false;
    DateTime StartGame = new DateTime();
    StartGame = DateTime.Now;
 
    Cell target = new Cell(coor, Generation, IsAlive, StartGame);
    Assert.AreEqual(target.StartGame_Id, StartGame);

}

/// <summary>
/// Test for IsAlive parameter
/// </summary>
[TestMethod()]
public void IsAliveTest()
{
    Coordinates coor = new Coordinates(1, 2);
    int Generation = 0;
    bool IsAlive = true;
    DateTime StartGame = new DateTime();
    StartGame = DateTime.Now;

    Cell target = new Cell(coor, Generation, IsAlive, StartGame);
    Assert.AreEqual(target.IsAlive, IsAlive);
}
    }
}
