﻿using GameOfLife.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Reflection;

namespace GameOfLife.Tests
{
    
    
    /// <summary>
    ///This is a test class for GenerationsModelTest and is intended
    ///to contain all GenerationsModelTest Unit Tests
    ///</summary>
    [TestClass()]
    public class GenerationsModelTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


/// <summary>
///A test for GenerationsModel Constructor
///</summary>
[TestMethod()]
public void GenerationsModelConstructorTest()
{
    //Arrange
    int GridWidth = 2; 
    int GridHeight = 2; 
    //Act
    GenerationsModel target = new GenerationsModel(GridWidth, GridHeight);
    //Assert
    Assert.IsNotNull(target);
   
}
/// <summary>
///A test for GenerationsModel Constructor when GridWidth parameter is 0
///</summary>
[TestMethod()]
[ExpectedException(typeof(ArgumentException),"Param must be greater than 0")]
public void GenerationsModelConstructorGridWidth0Test()
{
    //Arrange
    int GridWidth = 0;
    int GridHeight = 2;
    //Act
    GenerationsModel target = new GenerationsModel(GridWidth, GridHeight);

}
/// <summary>
///A test for GenerationsModel Constructor when GridHeight parameter is 0
///</summary>
[TestMethod()]
[ExpectedException(typeof(ArgumentException), "Param must be greater than 0")]
public void GenerationsModelConstructorGridHeight0Test()
{
    //Arrange
    int GridWidth = 2;
    int GridHeight = 0;
    //Act
    GenerationsModel target = new GenerationsModel(GridWidth, GridHeight);

}
/// <summary>
///A test for GenerationsModel Constructor to check that the grid length is created
///correctly
///</summary>
[TestMethod()]
public void GenerationsModelConstructorGridLengthTest()
{
    //Arrange
    int GridWidth = 2;
    int GridHeight = 3;
    //Act
    GenerationsModel target = new GenerationsModel(GridWidth, GridHeight);

    //Arrange
    PrivateObject ptarget = new PrivateObject(target);
    Grid grid = (Grid) ptarget.GetField("inputGrid",BindingFlags.Instance|BindingFlags.NonPublic);
    Assert.IsTrue(grid.GridWidth==2);
    Assert.IsTrue(grid.GridHeight == 3);


}

/// <summary>
///A test for NextGenerationGrid Block pattern no changes
///</summary>
[TestMethod()]
public void NextGenerationBlockPatternTest()
{
    //Arrange
    int GridWidth = 4;
    int GridHeight = 4;
    GenerationsModel target = new GenerationsModel(GridWidth, GridHeight);
    Coordinates[] ListInitialCells = new Coordinates[4];
    ListInitialCells[0] = new Coordinates(0, 0);
    ListInitialCells[1] = new Coordinates(0, 1);
    ListInitialCells[2] = new Coordinates(1, 0);
    ListInitialCells[3] = new Coordinates(1, 1);
    Game game = new Game(DateTime.Now);
    Cell[] CellList;
    Cell[] CellListActual;
    CellListActual = target.Start(ListInitialCells, game);
    target.UpdateGrid(CellListActual);
    //Act
    CellList=target.NextGenerationGrid();
    //Assert
    Assert.AreEqual(CellList.Length,0);   
}
/// <summary>
///A test for NextGeneration Blinker pattern 2 cells die 2 cells become live
///</summary>
[TestMethod()]
public void NextGenerationBlinkerPatternTest()
{
    //Arrange
    int GridWidth = 4;
    int GridHeight = 4;
    GenerationsModel target = new GenerationsModel(GridWidth, GridHeight);
    Coordinates[] ListInitialCells = new Coordinates[3];
    ListInitialCells[0] = new Coordinates(1, 0);
    ListInitialCells[1] = new Coordinates(1, 1);
    ListInitialCells[2] = new Coordinates(1, 2);
    Coordinates[] ListExpectedCells = new Coordinates[4];
    ListExpectedCells[0] = new Coordinates(0, 1);
    ListExpectedCells[1] = new Coordinates(1, 0);
    ListExpectedCells[2] = new Coordinates(1, 2);
    ListExpectedCells[3] = new Coordinates(2, 1);
    DateTime StartGame = DateTime.Now;

    Game game = new Game(StartGame);
    Cell[] CellList;
    Cell[] CellListActual;
    CellListActual = target.Start(ListInitialCells, game);
    target.UpdateGrid(CellListActual);
    //Act
    CellList = target.NextGenerationGrid();
    //Assert
    PrivateObject ptarget = new PrivateObject(target);
    Grid inputGrid = (Grid)ptarget.GetField("inputGrid", BindingFlags.Instance | BindingFlags.NonPublic);
    bool correct=false; 
    foreach (Cell cell in CellList)
    {
        correct = false;
        foreach (Coordinates coor in ListExpectedCells)
        {

            if (cell.Coor.X == coor.X && cell.Coor.Y == coor.Y)
            {
                correct = true;
            }
        }
        if (!correct) break;
    }
   
    Assert.IsTrue(correct);
    Assert.AreEqual(CellList.Length, 4);
}
/// <summary>
///A test for Start
///</summary>
[TestMethod()]
public void StartTest()
{
    //Arrange
    int GridWidth = 2; 
    int GridHeight = 2; 
    GenerationsModel target = new GenerationsModel(GridWidth, GridHeight); 
    Coordinates[] ListInitialCells= new Coordinates[2]; 
    ListInitialCells[0] =new Coordinates(0, 0);
    ListInitialCells[1] = new Coordinates(1, 0);
    Game game = new Game(DateTime.Now);
    Cell[] ListCell;
    //Act
    ListCell=target.Start(ListInitialCells,game);

    //Assert
    PrivateObject ptarget = new PrivateObject(target);
    Grid grid = (Grid)ptarget.GetField("inputGrid", BindingFlags.Instance | BindingFlags.NonPublic);
    Assert.IsFalse(grid[0, 0].IsAlive);
    Assert.IsFalse(grid[0, 1].IsAlive);
    Assert.IsFalse(grid[1, 0].IsAlive);
    Assert.IsFalse(grid[1, 1].IsAlive);
    Assert.IsTrue(ListCell[0].Coor.X == 0 && ListCell[0].Coor.Y == 0);
    Assert.IsTrue(ListCell[1].Coor.X == 1 && ListCell[1].Coor.Y == 0);
}
    }
}
