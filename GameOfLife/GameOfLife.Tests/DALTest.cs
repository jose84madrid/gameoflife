﻿using GameOfLife.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Reflection;

namespace GameOfLife.Tests
{
    
    
    /// <summary>
    ///This is a test class for DALTest and is intended
    ///to contain all DALTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DALTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


/// <summary>
///A test for DAL Constructor
///</summary>

[TestMethod()]
public void DALConstructorTest()
{
    //Arrange && Act
    DAL target = new DAL();
    //Assert
    Assert.IsNotNull(target);
}
/// <summary>
/// A test for the privare field "repository";
/// </summary>
[TestMethod()]
public void repositoryConstructorTest()
{
    //Arrange && Act
    DAL target = new DAL();
    //Assert
    PrivateObject ptarget = new PrivateObject(target);
    var x = ptarget.GetField("repository", BindingFlags.Instance | BindingFlags.NonPublic);

    Assert.AreEqual(target,x);
}

/// <summary>
///A test for SaveCell
///</summary>

[TestMethod()]
public void SaveCellTest()
{
    //Arrange
    DAL target = new DAL();
    DateTime GameStart = new DateTime();
    GameStart = DateTime.Now;
    Game game = new Game(GameStart);
    Coordinates coor1 = new Coordinates(1,1);
    Coordinates coor2 = new Coordinates(1, 2);
    Cell[] cell = new Cell[2];
    cell[0]=   new Cell(coor1, 0, false, GameStart);
    cell[1] =  new Cell(coor2, 0, false, GameStart);
    
    //Act
    target.SaveGame(game);
    target.SaveCell(cell);
    //Assert
    Assert.Inconclusive("A method that does not return a value cannot be verified.");
}
[TestMethod()]
[ExpectedException(typeof(ArgumentNullException), "The parameter cell cannot be null")]
public void SaveCellNullParameterTest()
{
    //Arrange
    DAL target = new DAL();
    Cell[] CellList = new Cell[1];
    Cell cell = null;
    CellList[0] = cell;
    //Act
    target.SaveCell(CellList);
    //Assert
    Assert.Inconclusive("A method that does not return a value cannot be verified.");
}

/// <summary>
///A test for SaveGame
///</summary>

[TestMethod()]
public void SaveGameTest()
{
    //Arrange
    DAL target = new DAL(); 
    DateTime GameStart = new DateTime();
    GameStart = DateTime.Now;
    Game game = new Game(GameStart);
    //Act
    target.SaveGame(game);
    //Assert
    Assert.Inconclusive("A method that does not return a value cannot be verified.");
}
[TestMethod()]
[ExpectedException(typeof(ArgumentNullException), "The parameter cell cannot be null")]
public void SaveGameNullParameterTest()
{
    //Arrange
    DAL target = new DAL();

    Game game = null;
    //Act
    target.SaveGame(game);
    //Assert
    Assert.Inconclusive("A method that does not return a value cannot be verified.");
}
    }
}
