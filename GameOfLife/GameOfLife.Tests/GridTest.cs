﻿using GameOfLife.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Collections.Generic;
using System.Reflection;

namespace GameOfLife.Tests
{
    
    
    /// <summary>
    ///This is a test class for GridTest and is intended
    ///to contain all GridTest Unit Tests
    ///</summary>
    [TestClass()]
    public class GridTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Grid Constructor
        ///</summary>
        [TestMethod()]
        public void GridConstructorTest()
        {
            //Arrange
            int GridWidth = 0; 
            int GridHeight = 0;
            //Act
            Grid target = new Grid(GridWidth, GridHeight);
            //Assert
            Assert.IsNotNull(target);
        }
        /// <summary>
        /// Check the variable initialization
        /// </summary>
        [TestMethod()]
        public void GridWidthGridHeighTest()
        {
            //Arrange
            int GridWidth = 1;
            int GridHeight = 2;
            //Act
            Grid target = new Grid(GridWidth, GridHeight);
            //Assert
            Assert.AreEqual(target.GridWidth, 1);
            Assert.AreEqual(target.GridHeight, 2);
            Assert.AreEqual(target.RowsList.Count, 0);
        }

        /// <summary>
        ///A test for Exception x gt GridWidth when getting Cell
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException),"Argument out of bound")]
        public void SetCellIndex_x_gt_GridHeightTest()
        {
            //Arrange
            int GridWidth = 2; 
            int GridHeight = 2; 
            Grid target = new Grid(GridWidth, GridHeight);
            int x = 3; 
            int y = 0; 
            Cell cell = null;
         
            //Act
            target[x, y] = cell;     
        }
                /// <summary>
        ///A test for Exception x lt zero GridWidth when getting Cell
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException),"Argument out of bound")]
        public void SetCellIndex_x_lt_zeroTest()
        {
            //Arrange
            int GridWidth = 2; 
            int GridHeight = 2; 
            Grid target = new Grid(GridWidth, GridHeight);
            int x = -1; 
            int y = 0; 
            Cell expected = null;
            Cell actual;
            //Act
            target[x, y] = expected;     
        }
        /// <summary>
        ///A test for Exception y gt GridWidth when getting Cell
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException),"Argument out of bound")]
        public void SetCellIndex_y_gt_maxwidthTest()
        {
            //Arrange
            int GridWidth = 2; 
            int GridHeight = 2; 
            Grid target = new Grid(GridWidth, GridHeight);
            int x = 0; 
            int y = 3; 
            Cell expected = null;
            Cell actual;
            //Act
            target[x, y] = expected;     
        }
                /// <summary>
        ///A test for Exception y lt GridWidth when getting Cell
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException),"Argument out of bound")]
        public void SetCellIndex_y_lt_zeroTest()
        {
            //Arrange
            int GridWidth = 2; 
            int GridHeight = 2; 
            Grid target = new Grid(GridWidth, GridHeight);
            int x = 0; 
            int y = -1; 
            Cell expected = null;
            Cell actual;
            //Act
            target[x, y] = expected;     
        }

        
        /// <summary>
        ///A test for Exception x gt GridWidth when getting Cell
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException),"Argument out of bound")]
        public void GetCellIndex_x_gt_GridHeightTest()
        {
            //Arrange
            int GridWidth = 2; 
            int GridHeight = 2; 
            Grid target = new Grid(GridWidth, GridHeight);
            int x = 3; 
            int y = 0; 
            //Act
            Cell expected = target[x, y];
              
        }
                /// <summary>
        ///A test for Exception x lt zero GridWidth when getting Cell
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException),"Argument out of bound")]
        public void GetCellIndex_x_lt_zeroTest()
        {
            //Arrange
            int GridWidth = 2; 
            int GridHeight = 2; 
            Grid target = new Grid(GridWidth, GridHeight);
            int x = -1; 
            int y = 0; 
            //Act
            Cell expected = target[x, y];    
        }
        /// <summary>
        ///A test for Exception y gt GridWidth when getting Cell
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException),"Argument out of bound")]
        public void GetCellIndex_y_gt_maxwidthTest()
        {
            //Arrange
            int GridWidth = 2; 
            int GridHeight = 2; 
            Grid target = new Grid(GridWidth, GridHeight);
            int x = 0; 
            int y = 3; 
            //Act
            Cell expected = target[x, y];   
        }
                /// <summary>
        ///A test for Exception y lt GridWidth when getting Cell
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException),"Argument out of bound")]
        public void GetCellIndex_y_lt_zeroTest()
        {
            //Arrange
            int GridWidth = 2; 
            int GridHeight = 2; 
            Grid target = new Grid(GridWidth, GridHeight);
            int x = 0; 
            int y = -1; 
             //Act
            Cell expected = target[x, y]; 
        }
    }
}
