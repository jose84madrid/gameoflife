﻿using GameOfLife.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace GameOfLife.Tests
{
    
    
    /// <summary>
    ///This is a test class for CoordinatesTest and is intended
    ///to contain all CoordinatesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CoordinatesTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


/// <summary>
///A test for Coordinates Constructor
///</summary>

[TestMethod()]
public void CoordinatesConstructorTest()
{
    //Arrange
    int x = 0; 
    int y = 0; 
    //Act
    Coordinates target = new Coordinates(x, y);
    //Assert
    Assert.IsNotNull(target);
}

/// <summary>
///A test for X
///</summary>
[TestMethod()]
public void XTest()
{
    //Arrange
    int x = 0; 
    int y = 0; 
    //Act
    Coordinates target = new Coordinates(x, y); 
    //Assert
    int expected = 0; 
    int actual;
    target.X = expected;
    actual = target.X;
    Assert.AreEqual(expected, actual);
}

/// <summary>
///A test for Y
///</summary>

[TestMethod()]
public void YTest()
{
int x = 0; 
int y = 0; 
Coordinates target = new Coordinates(x, y); 
int expected = 0; 
    int actual;
    target.Y = expected;
    actual = target.Y;
    Assert.AreEqual(expected, actual);
}
    }
}
