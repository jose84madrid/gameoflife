﻿using GameOfLife.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Collections.Generic;

namespace GameOfLife.Tests
{
    
    
    /// <summary>
    ///This is a test class for GameTest and is intended
    ///to contain all GameTest Unit Tests
    ///</summary>
    [TestClass()]
    public class GameTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


/// <summary>
///A test for Game Constructor
///</summary>

[TestMethod()]

public void GameConstructorTest()
{
    //Arrange 
    DateTime StartGame = DateTime.Now;
    //Act
    Game target = new Game(StartGame);
    //Assert
    Assert.IsNotNull(target);
}

/// <summary>
///A test for Startgame parameter
///</summary>

[TestMethod()]
public void StarGameTest()
{
    //Arrange 
    DateTime StartGame = DateTime.Now;
    //Act
    Game target = new Game(StartGame);
    //Assert
    Assert.AreEqual(target.StartGame_Id ,StartGame);
}
    }
}
